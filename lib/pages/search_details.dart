import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:searh_places_api/providers/search_details.dart';

class PlaceDetails extends StatefulWidget {
  final String placeId;

  const PlaceDetails({Key key, this.placeId}) : super(key: key);

  @override
  _PlaceDetailsState createState() => _PlaceDetailsState();
}

class _PlaceDetailsState extends State<PlaceDetails> {
  @override
  void initState() {
    super.initState();

    Future.microtask(() =>
        context.read<SearchDetailsProvider>().getPlaceDetails(widget.placeId));
  }

  @override
  Widget build(BuildContext context) {
    var state = context.watch<SearchDetailsProvider>();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Search Details",
        ),
        backgroundColor: Colors.teal[600],
        elevation: 50.0,
        leading: Icon(Icons.menu),
        bottom: PreferredSize(
            child: Icon(
              Icons.linear_scale,
              size: 60.0,
            ),
            preferredSize: Size.fromHeight(50.0)),
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.all(8.0),
        child: state.loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
            
                  Card(
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.location_city_rounded, size: 50),
                          title: Text("Name:  ${state.place.name}"),
                          
                          
                        ),
                      ],
                    ),
                  ),
         
                  SizedBox(
                    height: 12.0,
                  ),
                  Card(
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.location_on_rounded, size: 50),
                          title: Text("Latitude:  ${state.place.geometry.location.lat}"),
                          
                          
                        ),
                      ],
                    ),
                  ),
                   SizedBox(
                    height: 12.0,
                  ),
                  Card(
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.location_on_rounded, size: 50),
                          title: Text("Longitude:  ${state.place.geometry.location.lat}"),
                          
                          
                        ),
                      ],
                    ),
                  ),
      
                ],
              ),
      ),
    );
  }
}
