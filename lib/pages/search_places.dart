import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:searh_places_api/pages/search_details.dart';
import 'package:searh_places_api/providers/suggestion_detals.dart';

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextField(
              decoration: InputDecoration(hintText: 'Search'),
              onChanged: (value) {
                context.read<SuggestionProvider>().getPrediction(value);
                // print(value);
                //
              }),
        ],
      ),
    );
  }
}

class SearchPlaces extends StatefulWidget {
  @override
  _SearchPlacesState createState() => _SearchPlacesState();
}

class _SearchPlacesState extends State<SearchPlaces> {
  @override
  Widget build(BuildContext context) {
    var state = context.watch<SuggestionProvider>();

    //  MakeSearchRequest makeRequest = MakeSearchRequest();
    //  MakeSuggestionRequest suggestion = MakeSuggestionRequest();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Place Finder",      
        ),
        backgroundColor: Colors.teal[600],
        elevation: 50.0,
        leading: Icon(Icons.menu),
        bottom: PreferredSize(
            child: Icon(
              Icons.linear_scale,
              size: 60.0,
            ),
            preferredSize: Size.fromHeight(50.0)),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(30.0, 40.0, 10.0, 20.0),
          child: Column(
            children: [
              Search(),
              state.loading
                  ? Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      ),
                    )
                  : ListView.builder(
                      itemCount: state.predictions.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            print(state.predictions[index].placeId);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PlaceDetails(
                                          placeId:
                                              state.predictions[index].placeId,
                                        )));
                          },
                          child: Card(                            elevation: 6.0,
                            child: Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                         
                                  Card(
                                    child: Column(
                                      children: [
                                        ListTile(
                                       
                                          title: Text(
                                              "${state.predictions[index].description}"),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      })
            ],
          ),
        ),
      ),
    );
  }
}
