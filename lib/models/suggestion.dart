class Suggestion {

  List<Predictions> predictions = [];

  Suggestion(this.predictions);

  Suggestion.fromJson(Map<String, dynamic> json) {
  
      json['predictions'].forEach((prediction) {
        predictions.add(new Predictions.fromJson(prediction));
      });
 
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.predictions != null){
      data['predictions'] = this.predictions.map((prediction) => prediction.toJson()).toList();
      // print(data);
    }
      
    return data;
  }
}

class Predictions {
  String placeId;
  String description;


  Predictions(this.placeId, this.description);

  Predictions.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    placeId = json['place_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['place_id'] = this.placeId;
    return data;
  }
}
