
import 'package:flutter/material.dart';
import 'package:searh_places_api/http_requests/search_places.dart';
import 'package:searh_places_api/models/place.dart';

class SearchDetailsProvider with ChangeNotifier {
 Place _place;
  bool _loading = false;

  Place get place => _place;

  bool get loading => _loading;

  Future<void> getPlaceDetails(String placeId) async {
    _loading = true;
    notifyListeners();

    try {
      _place = (await fetchPlaces(placeId)).place;
      _loading = false;
      notifyListeners();
    } catch (e) {
    
      _loading = false;
      notifyListeners();
    }
  }
}
