
import 'package:searh_places_api/models/suggestion.dart';
import 'package:flutter/material.dart';
import 'package:searh_places_api/http_requests/display_suggestions.dart';

class SuggestionProvider with ChangeNotifier {
  List<Predictions> _predictions = [];
  bool _loading = false;


  List<Predictions> get predictions => _predictions;

  bool get loading => _loading;

  Future<void> getPrediction(String name) async {
    print(name);
    _loading = true;
    notifyListeners();
    try {
      _predictions = (await fetchSuggestionPlaces(name)).predictions;
      _loading = false;
      print(_predictions);
      notifyListeners();
    } catch (e) {
      //handle error
      _loading = false;
      notifyListeners();
    }
  }
}
