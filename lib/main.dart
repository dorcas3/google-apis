import 'package:flutter/material.dart';
import './pages/search_places.dart';
import 'providers/search_details.dart';
import 'providers/suggestion_detals.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => SuggestionProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => SearchDetailsProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => SearchPlaces(),
        },
      ),
    );
  }
}
