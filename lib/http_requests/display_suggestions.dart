import 'dart:convert';
import 'package:http/http.dart';
import '../models/suggestion.dart';
import '../credentials/credentials.dart';

String apiKey = PLACES_API_KEY;

// List<Suggestion> _displayResults = [];

Future<Suggestion> fetchSuggestionPlaces(String input) async {
  final response = Uri.parse(
      'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&key=$apiKey&sessiontoken=1234567890&components=country:ke');
  final res = await get(response);
  // print(res.body);
  return (Suggestion.fromJson(json.decode(res.body)));
}
