import 'dart:convert';
import 'package:http/http.dart';
import '../models/place.dart';
import 'package:searh_places_api/models/place.dart';
import 'package:searh_places_api/credentials/credentials.dart';


  String apiKey = PLACES_API_KEY;

  Future <SearchResults>fetchPlaces(String inputId) async {
    final response = Uri.parse(
        'https://maps.googleapis.com/maps/api/place/details/json?placeid=$inputId&key=$apiKey');
       

    final res = await get(response);
    return SearchResults.fromJson(json.decode(res.body));

    // var body = jsonDecode(res.body);

    // print(body);
    // return body;
  
}