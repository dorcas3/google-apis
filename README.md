# search_places_api

A new Flutter project.

## Description

A flutter Application that uses Google Api to search a place and display name, latitude and longitude of the place

## Screenshots Preview

![Image Preview](assets/images/home.png)  ![Image Preview](assets/images/home2.png)



## Getting Started


### User Requirements

- Build a simple form with a search field which will list the places that match the input text as provided by the google API.
- For the details you will need to display;
    - Name of the location
    - Longitude and latitude of the location

## Prerequisites
- Flutter v2.7.0

## Setup and Installations

- Clone the repository into your local machine
- In the terminal type flutter doctor to install all the dependencies
- Type flutter run command to launch the application

### License

* LICENSED UNDER  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](license/MIT)
